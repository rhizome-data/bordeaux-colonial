---
layout: default
title: La carte des lieux
---

<div class="post">
	<h1 class="pageTitle">La carte des lieux</h1>
	<p class="intro">Nous avons référencé sur une carte les lieux décrits dans le guide ou qui le seront dans les articles.</p>

	<iframe width="100%" height="600px" frameBorder="0" allowfullscreen src="https://framacarte.org/fr/map/guide-rues-de-bordeaux-colonial_49821?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe><p><a target="_new" href="https://framacarte.org/fr/map/guide-rues-de-bordeaux-colonial_49821">Voir en plein écran</a></p>
</div>
