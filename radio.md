---
layout: default
title: Les émissions sur la Clé des ondes
---

<div class="post">
	<h1 class="pageTitle">Les émissions sur la Clé des ondes</h1>
  <img class="center" src="{{ '/assets/img/Cle_des_ondes.jpg' | prepend: site.baseurl }}" alt="">
	<p class="intro">Emissions animées par le collectif Sortir du colonialisme 33, soutenu par la Clé des Ondes, le Mrap 33, l'UJFP Aquitaine, l'association Pourquoi Pas 33, Cauri, RESF .....</p>

<p> Réunis en collectif militant composé de citoyens et soutenus par des associations, nous travaillons autour de la mémoire. Et plus particulièrement de la mémoire du nom des rues et de leur histoire sur Bordeaux métropole. Notre objectif est d'aboutir un ouvrage reprenant ces histoires masquées, oubliées, cachées, et qui indirectement constituent un discours silencieux et insidieux, justifiant aujourd'hui encore les ferments d'une idéologie coloniale et raciste.</p>

<p> A Bordeaux, le travail d'historiens et d'associations anti-esclavagistes a mis en évidence des rues portant le nom de familles esclavagistes. Ce combat est aussi le notre, et nous voulons ouvrir notre propos plus largement autour du passé colonial. Ce combat n'est pas concurrentiel mais complémentaire.</p>

<p>C'est ce travail que notre mission illustre chaque semaine avec ses invités, ses chroniques, et son agenda.

Le plus souvent possible, nous invitons en direct un invité et nous présentons l'actualité des associations partenaires et amies.</p>


	<p><a target="_new" href="https://lacledesondes.fr/emission/le-guide-du-bordeaux-colonial">Retrouvez toutes les émissions sur la page dédiée de la Clé des ondes </a></p>

</div>
