---
layout: default
title: Recherche dans le guide du Bordeaux colonial et de la métropole bordelaise
---

<!-- Html Elements for Search -->
<div id="search-container">
<p>Vous pouvez utiliser le champ de recherche ci-dessous afin de rechercher dans les titres et les descriptions des articles de ce site.</p>
<input type="text" id="search-input" placeholder="recherche...">
<ul class="noList" id="results-container"></ul>
</div>

<!-- Script pointing to search-script.js -->
<script src="/assets/js/search.min.js" type="text/javascript"></script>

<!-- Configuration -->
<script>
SimpleJekyllSearch({
searchInput: document.getElementById('search-input'),
resultsContainer: document.getElementById('results-container'),
json: '/search.json'
})
</script>
