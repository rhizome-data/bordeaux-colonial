---
layout: default
title: A propos de ce Guide
---

<div class="post">
	<h1 class="pageTitle">A propos de ce Guide</h1>
	<img class="center" src="{{ '/assets/img/logo-blanc.png' | prepend: site.baseurl }}" alt="">
	<p class="intro">Ce Guide du Bordeaux colonial et de la métropole prend l’Histoire de la ville, en la braquant sur les noms de rues et autres voies et lieux choisis pour honorer ceux qui ont contribué à la construction de la France coloniale.</p>
	<p>L’État en France s’est constitué comme État colonial et, Bordeaux s’est développé au rythme de la constitution et la gestion de cet Empire. C’est ce que nous tâchons de rendre visible par ce site internet et l'ouvrage à paraitre qui se veulent didactiques en interrogeant de quelle façon les noms que portent les voies et places de la ville sont témoins de cette histoire et de ce rôle assumés par les édiles dans leur dénomination.

Ce n’est pas parce que négriers, esclavagistes, sabreurs, administrateurs coloniaux, théoriciens du racisme que beaucoup de personnalités ont été honorées. Mais, il ressort que grand nombre d'entre elles ont été clairement engagées dans ce système. Bien des bienfaiteurs de la ville ont fait ruisseler un peu de la fortune accumulée par la production et le négoce des produits coloniaux issus de l’esclavage et du travail forcé. Bien des militaires et des politiques honorés ont contribué à leur ouvrir et à protéger leurs marchés. Bien des universitaires leur ont apporté la caution scientifique justifiant la domination.
</p>

<p>Que ce guide permette de voir la magnificence de la ville sous un autre jour. Qu’il invite à d’autres promenades. Qu’il contribue à décoloniser les imaginaires.

Ce modeste guide est un produit de haute nécessité dans la lutte contre toutes les formes de racisme.</p>

<p>Il doit être entendu que la place accordée à tel personnage ou à tel lieu n’est pas proportionnelle à leur importance. Nous sommes convaincus que notre travail comporte des erreurs et des lacunes. Par exemple, beaucoup de rues à Bordeaux portent le nom des propriétaires de terrains qui ont été viabilisés et lotis, sans que l’on en sache plus sur les origines de leur fortune.</p>

<p>Nous remercions par avance les lecteurs attentifs qui <a target="_new" href="https://www.bordeaux-colonial.fr/contact/">nous écriront</a> pour rectifier ou compléter nos informations – d’où l’intérêt de notre site internet. La prochaine édition n’en sera que plus exacte.</p>



	<h2>Qui sommes nous ? </h2>
	<p>Le collectif a d’ores et déjà reçu l’adhésion d’associations et de personnes qui s’y engagent à titre personnel.</p>

<p>Associations :</p>

<ul>
	<li><a target="_new" href="https://www.ancrage.org/">Ancrage en partage </a></li>
	<li>Cercle d’Etudes Sociales de Bordeaux</li>
	<li><a target="_new" href="https://lacledesondes.fr/">La Clef des Ondes</a></li>
	<li><a target="_new" href="https://espacesmarxaquitainebordeauxgironde.wordpress.com/">Espaces Marx Aquitaine Bordeaux Gironde</a></li>
	<li><a target="_new" href="http://ldh-gironde.org/">Ligue des Droits de l’Homme Bordeaux et Gironde</a></li>
	<li>MRAP 33</li>
	<li><a target="_new" href="http://palestine33.free.fr/">Palestine 33</a></li>
	<li><a target="_new" href="https://assopourquoipas.org/">Pourquoi Pas</a></li>
	<li><a target="_new" href="https://survie.org/">Survie</a></li>
	<li><a target="_new" href="https://ujfp.org/">l’Union Juive Française pour la Paix - Aquitaine</a></li>
</ul>

<p>Personnes</p>

	<ul>
		<li>Gérard Clabé</li>
  	<li>Delphine Jamet</li>
  	<li>Guy Lenoir</li>
  	<li>Jean-François Meekel</li>
  	<li>Sandra Merlet</li>
    <li>Annie Najim</li>
  	<li>Cheikh Sow</li>
  	</ul>
</div>
