---
layout: default
title: Contact Long Haul
---

<div id="contact">
  <h1 class="pageTitle">Contactez-nous</h1>
  <div class="contactContent">
    <p class="intro">Vous pouvez nous contacter par email en utilisant ce formulaire</p>
  </div>
<form action="https://formspree.io/xqkpzzyo" method="POST">
  <label>
    Votre email:
    <input type="text" name="_replyto">
  </label>
  <label>
    Votre message:
    <textarea name="message"></textarea>
  </label>

  <!-- your other form fields go here -->

  <button type="submit">Envoyer</button>
