---
layout: default
title: Le guide du Bordeaux colonial et de la métropole bordelaise
---

<div class="post">
	<h1 class="pageTitle">Le guide du Bordeaux colonial et de la métropole bordelaise</h1>
  <img class="center" src="{{ '/assets/img/couverture-guide.png' | prepend: site.baseurl }}" alt="">
	<p class="intro"><span class="dropcap">B</span>ordeaux s’est développé en jouant un rôle essentiel dans la constitution de l’Empire français.</p>

<p>Ce livre s’intéresse à l’histoire de la ville à travers les noms de rues, voies et autres lieux choisis pour honorer ceux qui ont contribué à la construction de la France coloniale. Ce n’est pas, le plus souvent, en tant que négriers, esclavagistes, sabreurs, administrateurs coloniaux, théoriciens du racisme que beaucoup de personnalités ont été honorées. Elles l’ont été pour d’autres raisons mais elles ont été clairement engagées dans le système colonial. Bien des bienfaiteurs de la ville ont fait ruisseler un peu de leur fortune accumulée par la production et le négoce des produits coloniaux issus de l’esclavage et du travail forcé. Bien des militaires et des hommes politiques honorés ont contribué à leur ouvrir et à protéger leurs marchés. Bien des universitaires ont apporté la caution scientifique justifiant la domination.</p>

<p>Ce guide n’ignore pas les quelques anticolonialistes à qui une place a tout de même été faite dans la ville.</p>

<p>Il visite quelques lieux de mémoire et propose quelques coups de projecteur sur des aspects peu enseignés de l’histoire coloniale.</p>

<p>250 pages relatant une histoire encore vivante, cachée, refoulée par l'histoire officielle. 

Initialement prévue avec une centaine de pages, nous en sommes arrivés à 250 ..... et quelques clichés ou dessins, avec en couverture un aimable et pertinent dessin réalisé par Jacques Tardi (un grand merci),

et un prix qui reste fixé à 10 euros.....

Passez déjà vos commandes et réservations en nous écrivant sur le guidedubordeauxcolonial@gmail.com</p>

<p>Que ce guide permette de voir la magnificence de la ville sous un autre jour. Qu’il invite à d’autres promenades. Qu’il contribue à décoloniser les imaginaires.</p>

<p>C’est un produit de haute nécessité́ dans la lutte contre toutes les formes de racisme.</p>

<p>Éditions Syllepse, prochainement dans toutes les bonnes librairies</p>

<p>                   .................... </p>


<h1 class="pageTitle">La presse en parle</h1>

<p>Article de la revue "Ecole émancipée", sept-oct 2020
  <img class="center" src="{{ '/assets/img/Guide_dans_Revue_EE_001.jpg' | prepend: site.baseurl }}" alt=""></p>
  

<p>Article paru dans Sud-Ouest, 27 juin 2020
  <img class="center" src="{{ '/assets/img/Sud_Ouest_20200627.jpg' | prepend: site.baseurl }}" alt=""></p>


<p>Communiqué de presse de la radio La Clé des Ondes
  <img class="center" src="{{ '/assets/img/communiqué_la_clé_des_ondes.jpg' | prepend: site.baseurl }}" alt=""></p>


<p>Article dans "la gazette de l'Utopia", juin 2020
  <img class="center" src="{{ '/assets/img/gazette_utopia.jpg' | prepend: site.baseurl }}" alt=""></p>


<p>Forum de l'association 4ACG (Anciens Appelés en Algérie et leurs Ami(e)s Contre la Guerre
  <img class="center" src="{{ '/assets/img/forum_4ACG_20200603.jpg' | prepend: site.baseurl }}" alt=""></p>


<p>Article de "la Libre Pensée Girondine", mai 2020
  <img class="center" src="{{ '/assets/img/article_Libre_Pensée_Girondine_mai2020.jpg' | prepend: site.baseurl }}" alt=""></p>


<p>article dans l'édition numérique du journal Sud-Ouest, 19 mai 2020
  <img class="center" src="{{ '/assets/img/article_Sud-OUest_edition-20200519.jpg' | prepend: site.baseurl }}" alt=""></p>

</div>
