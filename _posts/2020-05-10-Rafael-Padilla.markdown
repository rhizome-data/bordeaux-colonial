---
layout: post
title:  "Rafael Padilla dit le clown Chocolat"
date:   2020-05-13
image: rafael-padilla.jpg
---

<p class="intro"><span class="dropcap">R</span>afael Padilla, dit le clown Chocolat, est né à Cuba vers 1868. Vendu comme esclave en Espagne, il est mort à Bordeaux, le 4 novembre 1917 et enterré au cimetière protestant, dans le carré des indigents. </p>

Rafael Padilla devint le premier grand artiste noir en France. Connu dans le monde circassien pour ses «entrées comiques» avec Foottit, il subit les humiliations sociales, raciales de l’époque coloniale, tout en rencontrant un succès retentissant dans le Paris de la Belle Époque. Peint par Toulouse-Lautrec, commenté par Jean Cocteau, inspirant Debussy, Rafael fut aussi un bon père de famille. Lui, l’orphelin, n’oublia jamais la solitude et les humiliations de son enfance, devenant le premier clown hospitalier, pour soulager par le rire la souffrance des petits malades.

Redécouvert par l’historien Gérard Noiriel, l’association PourQuoiPas 33, de nombreuses personnalités et associations bordelaises ont milité pour une reconnaissance officielle du clown Chocolat par la ville de Bordeaux. C’est en 2017 que fut inaugurée officiellement par son maire Alain Juppé l’aire des cirques de passage rive droite (face au 86 du quai des Queyries), du nom de Rafael Padilla, dit le clown Chocolat. Au cimetière protestant une plaque et une allée portent aussi son nom.


<p>Retrouvez toute l'activité de l'association <a target="_new" href="https://assopourquoipas.org/amis-clown-chocolat/">PourQuoiPas</a></p>

<p>Retrouvez en podcast l'ITW de Stéphanie Anfray FCPE 33 - Aire Rafael Padilla - Contre sommet et conseil municipal du 02/03 <a target="_new" href="https://lacledesondes.fr/emission/le-guide-du-bordeaux-colonial">diffusée sur la Clé des ondes le 11 mars 2020</a></p>
