---
layout: post
title:  "Ernest Renan (rue) "
date:   2020-08-26
image: Ernest_Renan.jpg
---

Ecrivain, philologue, philosophe et historien, professeur au Collège de France (dès 1862), élu à l’académie Française (1878), commandeur (1884) et même grand officier (1888) de la Légion d’honneur, Ernest Renan (1823-1892) est un des grands penseurs de la III° République, une référence pour Paul Bert* comme pour Jules Ferry*.

Il est aujourd’hui encore abondamment cité pour sa conception de la nation comme libre association d’individus ayant un passé commun (« avoir fait de grandes choses ensemble, vouloir en faire encore »).
Mais est souvent oublié que cette conception qui vaut pour les peuples « civilisés » ne vaut pas pour les races inférieures. 

Ernest Renan sera un chaud partisan de la colonisation : « Une nation qui ne colonise pas est irrévocablement vouée au socialisme, à la guerre du riche et du pauvre (…) La conquête d’un pays de race inférieure par une race supérieure qui s’y établit pour gouverner n’a rien de choquant. »
Olivier Le Cour Grandmaison, dans son livre « Ennemis mortels- représentations de l’islam et politiques musulmanes en France à l’époque coloniale » le cite en montrant comment il est véritablement le père d’une islamophobie savante : »Toute personne un peu instruite des choses de notre temps voit clairement la nullité intellectuelle des races qui tiennent uniquement de cette religion leur culture et leur éducation. Tous ceux qui ont été en Orient ou en Afrique sont frappés de ce qu’a de fatalement borné l’esprit d’un vrai croyant,de cette espèce de cercle de fer qui entoure sa tête, la rend absolument fermée à la science, incapable de ne rien apprendre ni de s’ouvrir à aucune idée nouvelle. »

L’islamophobie en ce début de XXI° siècle puise encore à cette source !

