---
layout: post
title:  "Toussaint Louverture (1748 - 1793)"
date:   2020-05-10
image: toussaint-louverture.JPG
---

<p class="intro"><span class="dropcap">O</span>ffert en 2005 par la République d’Haïti à la ville de Bordeaux à l’occasion du Bicentenaire de l’indépendance haïtienne, le buste de Toussaint Louverture, œuvre du sculpteur haïtien Ludovic Booz, prend place dans le square qui porte son nom depuis 2003. </p>

Le square Toussaint-Louverture est situé le long de la rive droite de la Garonne, dans le parc des Angéliques, face au lieu emblématique de la traite négrière à Bordeaux, le quai des Chartrons.

Héros de la Révolution de Saint-Domingue (appellation de la colonie française jusqu’en 1803), précurseur de la guerre d’indépendance et de la nation haïtienne, François-Dominique Toussaint, naît vers 1743 sur l’habitation de Bréda, nom qu’il portera à son affranchissement (vers 1776).

Propriétaire d’une plantation de café et de 13 esclaves, médecin grâce à sa connaissance des plantes qu’il tiendrait de son père, prince de l’Allada (Bénin), Toussaint de Bréda s’engage progressivement dans l’insurrection déclenchée au Nord de l’Île en 1791.

Surnommé « L’ouverture » pour son habileté à créer des brèches lors des combats, Toussaint prend ce nom à partir de 1793. Ses qualités de stratège et son talent d’organisateur, lui font prendre une place centrale et déterminante dans la conduite de la guerre de libération d’abord aux cotés des insurgés du Bois Caïman (22 août 1791), puis des espagnols installés dans l’autre partie de l’Île (aujourd’hui la République dominicaine), ensuite avec les abolitionnistes républicains, puis contre les mulâtres du Sud, et finalement contre l’expédition napoléonienne dirigée par le général Leclerc, expédition destinée à mettre fin à l’organisation de l’émancipation politique et économique de Saint-Domingue conduite par Toussaint Louverture.  

Assigné à résidence après sa reddition en mai 1802, il est capturé par traîtrise et déporté avec sa famille en France. Enfermé au fort de Joux dans le Doubs, sous une surveillance et dans un isolement toujours plus renforcé, il succombe le 7 avril 1803 à une apoplexie.

Une légende non confirmée disait ses restes transportés secrètement à Bordeaux par l’entremise de son fils, Isaac Louverture, installé dans ce port colonial depuis la levée de sa résidence surveillée à Agen. Isaac choisit Bordeaux pour les facilités de communication avec Haïti. Aujourd’hui, une plaque municipale signale l’immeuble où résidaient Isaac Louverture et son épouse, Louise Chancy, au 44 rue Fondaudège. La plaque est inaugurée par Alain Juppé, maire de Bordeaux, le 4 avril 2003, deux cents ans presque jour pour jour après le décès de Toussaint Louverture. Son fils Isaac repose au cimetière de la Chartreuse à Bordeaux dans le caveau de la famille Gragnon-Lacoste.




<p>Retrouvez en podcast Voyage d'Isabelle en Palestine #3 - Le Square Toussaint l'Ouverture <a target="_new" href="https://lacledesondes.fr/emission/le-guide-du-bordeaux-colonial">diffusé sur la Clé des ondes le 8 janvier 2020</a></p>
