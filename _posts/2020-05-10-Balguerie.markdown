---
layout: post
title:  "Pierre Balguerie-Stuttenberg (1778-1825)"
date:   2020-05-10
image: Balguerie-Stuttenberg.jpg
---

<p class="intro"><span class="dropcap">P</span>ierre Balguerie-Stuttenberg (1778-1825) est issu d’une famille de négociants coloniaux, ruiné par l’indépendance de Saint-Domingue, qui devient Haïti, la première République noire en 1804. Suivant les traces de son père, et lui-même doté d’un sens aigu des affaires, Pierre Balguerie-Stuttenberg devient un des négociants et armateurs bordelais les plus importants.</p>

Il épouse Sophie-Suzanne Stuttenberg, fille d’un puissant négociant en vins, en 1809. Par la suite, il fonde la société Balguerie, Sarget et la compagnie en s’associant avec le baron Jean-Auguste Sarget et David-Jean Verdonnet. Le but de cette compagnie est d’étendre le commerce maritime à celui du vin et de la bourse, en se reposant sur les denrées coloniales et l’armement de navire.

Rapidement, la société devient le premier pavillon français dans l’Inde; ouvrant la route des échanges au Bengale, Cochinchine et en Chine. Mais aussi en Amérique du Sud, la société y érige des bâtiments au Brésil, Chili et Pérou. Balguerie-Stuttenberg s’intéresse aussi à la traite négrière, il se pose en héritiers des négriers du XVIIIe siècle. En décembre 1814, il fait construire un navire négrier, nommé l’Africain. Comme le rapporte Eric Saugéra, dans Bordeaux, port négrier XVII-XIXe siècle, son équipage est composé d’homme aguerris dans la traite : « Le capitaine […] Antoine Plassiard avait le goût du commerce négrier. […] En 1803, sur un autre navire, il avait effectué une expédition négrière qui l’avait conduit à la Nouvelle-Orléans ». De plus, Balguerie-Stuttenberg est aussi un soutien des empires coloniaux, en effet il fournit au roi d’Espagne, en 1821, 40 vaisseaux pour écraser l’insurrection dans ses colonies américaines.

Pierre Balguerie-Stuttenberg est un haut nom de l’histoire maritime bordelaise, ayant fondé une des maisons les plus prospères. Néanmoins, par ses activités, apparaît d’autant plus clairement la relation noueuse entre Bordeaux et les colonies, ainsi que sa place centrale dans la traite.


Date de dénomination : 1864 puis 19/04/1901
