---
layout: post
title:  "René Maran (place)"
date:   2020-05-10
image: rene-maran.JPG
---

<p class="intro"><span class="dropcap">R</span>ené Maran, homme de lettre qui a marqué son époque, est né à Fort-de-France (Martinique) en 1887, et décède à Paris en 1960.</p>

Ses parents, d’origine gabonaise, l’envoient en pension dès l’âge de sept ans au lycée de Talence, puis au lycée Michel-de-Montaigne à Bordeaux. Après des études de droit à Bordeaux, il quitte la Gironde en 1910 et devient en 1912 administrateur en outre-mer et plus particulièrement en Oubangui-Chari, territoire français en Afrique centrale, aujourd’hui République centrafricaine.

L’œuvre qui l’inscrit au panthéon des hommes de lettres noirs les plus illustres, est publié en 1921: il s’agit de son roman Batouala parut aux éditions Albin Michel. Maran est un homme engagé dans la dénonciation du colonialisme et n’ayant en guise d’armes que des mots pour combattre ce fléau, il fait de son livre un char d’assaut face au colonialisme. Il y décrit les mœurs et traditions d’une tribu du point de vue du chef éponyme, dans un style naturaliste, et où les personnages principaux sont des Noirs. Jamais jusqu’ici les Noirs n’avaient eu de rôle principal dans un ouvrage. C’est en ce sens qu’on lui attribue le titre de précurseur du mouvement que l’on appelle la négritude.

Cependant, ce qui est nouveau, souvent, effraie. Et en 1921, la France littéraire se réveille stupéfaite, René Maran se voit attribuer le prix Goncourt, il est le premier homme noir à recevoir cette récompense, ce qui attise le courroux des critiques littéraires. On peut y lire: «On réhabilite à bien juste titre les malheurs nègres qui pullulent dans la littérature» (L’Excelsior). Et ce n’est là qu’une occurrence de ce que l’on peut trouver comme critique à l’égard de son œuvre. Ce roman fascine comme il peut mettre mal à l’aise la bonne conscience métropolitaine. Il parle des femmes vendues, de l’alcoolisme des colons et de la famine en Oubangui-Chari.

«Une œuvre écrite par un Noir et qui parle des Noirs» ainsi peut-on qualifier le roman de René Maran, à une époque où le racisme ordinaire se répand progressivement dans les attitudes métropolitaines. Homme noir et administrateur colonial, sa position vis-à-vis des évènements qu’il décrit est paradoxale puisqu’il dénonce un système auquel, malgré lui, il participe. La parution de l’ouvrage fait l’effet d’une bombe, il est contraint de démissionner de son poste d’administrateur colonial. Mais il continue son activité de journaliste et d’écrivain à Paris où il résidera par la suite.
Homme de lettres, engagé contre la colonisation, il s’inscrit dans la lignée de cette intelligentsia noire à l’instar d’Aimé Césaire et Léopold Sédar Senghor.

Aujourd’hui, la seule trace perceptible de cet illustre écrivain dans le paysage bordelais se résume à son nom donné à une place commerçante dans le centre-ville.

<blockquote>« Civilisation, civilisation, orgueils d’Européens et leurs charniers d’innocents. Rabindranath Tagore, le poète hindou, un jour à Tokyo, a dit ce que tu étais ! Tu bâtis ton royaume sur des cadavres. Quoi que tu veuilles, quoi que tu fasses, tu te meus dans le mensonge. À ta vue, les larmes de sourdre et la douleur de crier.Tu es la force qui prime le droit.Tu n’es pas un flambeau mais un incendie.Tout ce à quoi tu touches, tu le consumes » (Batouala).</blockquote>

Date de dénomination de la place : 20 mai 1966
