---
layout: post
title:  "Pont François Mitterrand"
date:   2020-01-07
image: pont_francois-mitterrand_bordeaux.jpg
---

<p class="intro"><span class="dropcap">L</span>es travaux de construction ont débuté en mars 1992.</p>

Alors nommé pont d'Arcins en raison de sa proximité avec l'île d'Arcins, l'ouvrage est inauguré le 7 décembre 1993 par le président de la République de l'époque, François Mitterrand. Son ouverture permet d’achever le contournement de l'agglomération, joignant Bègles à Bouliac. En 1997, le pont est rebaptisé François-Mitterrand en hommage à l'ancien Président qui l'avait inauguré et qui est décédé l'année précédente. Nous ne ferons pas une biographie complète de François Mitterrand ; sa jeunesse de catholique nationaliste et son entrée dans la Résistance après une période vichyste sont connues. Pendant la 4ème République, François Mitterrand dans ses différents mandats de parlementaire et de ministre (France d’Outre-Mer, Intérieur, Garde des Sceaux), est un colonialiste libéral, favorable à plus d’autonomie aux populations d’Indochine, de Tunisie ou du Maroc En revanche, en 1954 au moment du début de l’insurrection en Algérie ses propos de Ministre de l’Intérieur sont clairs : « La rébellion algérienne ne peut trouver qu'une forme terminale : la guerre. », « L'Algérie, c'est la France. »


### Réseau Pasqua et Françafrique

Chargé en 1956  par le Conseil des ministres de défendre le projet de loi remettant les pouvoirs spéciaux à l'armée, il donnera systématiquement son aval, en tant que Garde des Sceaux, aux nombreuses sentences de mort prononcées par les tribunaux d'Alger contre des militants de la lutte pour l'indépendance, notamment Fernand Iveton, membre du Parti communiste algérien (PCA), guillotiné à Alger le 11 février 1957. Sous son ministère, quarante-cinq militants algériens sont condamnés à mort de manière souvent expéditive pour lesquels Mitterrand ne donne que sept avis favorables à la grâce, et il couvrira le recours à la torture.


Comme Président de la République de 1981 à 1995, il maintiendra la logique de domination sur le continent africain comme dans les départements et territoires d’outre-mer. Il désignera son fils Jean-Christophe Mitterrand, au poste d’adjoint au conseiller de l’Elysée pour les affaires africaines, puis comme conseiller. J.C. Mitterrand sera convaincu d’avoir participé aux réseaux Pasqua de la Françafrique et d’en avoir tiré profit. Pour le premier septennat, on peut citer en août 1983, l’opération Manta renforçant l'intervention française au Tchad, en 1988 l’assaut de la grotte d’Ouvéa en Nouvelle Calédonie.


Pour le second septennat, surtout entre 1990 et 1994, une politique de soutien au Hutu Power au Rwanda et au gouvernement génocidaire (opération Amaryllis en avril 1994). La France n’a jamais voulu reconnaître sa responsabilité dans ce génocide, ni le fait que l’opération Turquoise avait pour but d’en exfiltrer les responsables (juin-août 1994). Si ces derniers événements se déroulent alors que la France vit avec un gouvernement de cohabitation (Balladur Premier Ministre, Léotard à la Défense, Juppé aux Affaires étrangères), c’est de l’Elysée que les décisions régaliennes sont prises. François Mitterrand aura donc été un acteur majeur dans la transition de la France de l’Union française à la Françafrique d’une décolonisation néocoloniale.
