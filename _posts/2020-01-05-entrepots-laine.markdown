---
layout: post
title:  "Les entrepôts Lainé"
date:   2020-01-07
image: entrepot_Laine.jpg
---

<p class="intro"><span class="dropcap">L</span>es entrepôts Lainé symbole de la richesse coloniale de Bordeaux</p>


En 1824, sur le territoire d’une dent creuse du tissu urbain laissée par la destruction du Château-Trompette sont érigés deux entrepôts que l’on nomme Lainé, en référence à Joseph-Henri-Joachim Lainé<sup id="ap1"> <a href="#nt1">1</a></sup>, Bordelais, issu d’une famille de négociants esclavagistes. Ministre de l’Intérieur lors du lancement des travaux, il les appuya de toute  son autorité à Paris , ce qui lui gagna l’honneur de donner son nom aux entrepôts. A l’origine du projet, précisément un entrepôt réel des denrées coloniales : la volonté de Pierre Balguerie-Stuttenberg. Négociant enrichi par le commerce du vin, par la spéculation sur les denrées coloniales et l’armement des navires<sup id="ap2"> <a href="#nt2">2</a></sup>, il fit pression sur la chambre de commerce afin de créer un espace capable de stocker sous douane (le sens de réel) les  marchandises arrivant des colonies et  issues du commerce triangulaire : sucre, cacao, café, coton, épices etc. en attente d’être réexpédiées vers l’Europe du Nord en particulier. Construits en deux rapides années, confiés à l’ingénieur Claude Deschamps qui venait d’achever l’édification du Pont de Pierre, les entrepôts Lainé remplirent  leur fonction jusqu’au début du 20ème siècle, concurrencés alors  par les hangars construits sur les quais. Un premier entrepôt est détruit en 1965, le second racheté par la ville en 1973 abrite dorénavant le CAPC, le musée d’art  contemporain de Bordeaux et Arc-en-rêve, centre d’architecture.  

<div style="style_de_notes">
<p id="nt1"><sup><a href="#ap1">1 </a> Joachim Lainé a également donné son nom à la place Lainé, le parvis de la Bourse Maritime, entre quai et entrepôts Lainé. A ce titre il aura droit à sa fiche car ce fut l’un des plus farouches opposants à l’interdiction de la traite.
</p>
</div>

<div style="style_de_notes">
<p id="nt2"><sup><a href="#ap2">2 </a>la  seule expédition négrière à laquelle il tenta de participer en armant l’Africain capota du fait de l’interdiction de la traite décrétée par la deuxième restauration en 1815.
</p>
</div>
