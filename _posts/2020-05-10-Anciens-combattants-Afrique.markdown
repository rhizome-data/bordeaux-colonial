---
layout: post
title:  "Rue des Anciens Combattants d’Afrique"
date:   2020-05-10
image: anciens-combattants-afrique1.JPG  

image: anciens-combattants-afrique2.JPG

---

<p class="intro"><span class="dropcap">L</span>e nom de la rue des Anciens Combattants d’Afrique est choisi, en 2010,  dans un contexte de commémoration du 70e anniversaire de l’Appel du 18 juin 1940, et le Cinquantenaire des Indépendances.</p>

La dénomination des Anciens Combattants d’Afrique englobe les troupes coloniales, créées en 1900, et composées de l’infanterie coloniale (« la Coloniale Blanche ») ainsi que les tirailleurs dits indigènes. En 1914, on compte plus de 600 000 soldats coloniaux, dont la moitié viennent du Maghreb et d’Afrique subsaharienne (les « tirailleurs sénégalais »). Les troupes coloniales sont reformées dès 1938, et représentent environ le quart des forces françaises. Les combattants d’Afrique sont notamment reconnus pour avoir participé au Débarquement de Provence (15 août 1944). 
Néanmoins, malgré leur aide manifeste aux combats métropolitains, la France ne leur pas a accordé des traitements égaux. Que ce soit par une différence de rations, de tenues ; ainsi que le blanchiment des troupes lors de la victoire. De plus, les tirailleurs sénégalais ont ensuite été victimes de la violence de l’État français : parqués dans des camps en Provence, rapatriés dès novembre 1944 au Sénégal. Par le refus des autorités de leur verser leur solde, les tirailleurs se révoltent et cela se conclue par le massacre du camp de Thiaroye (1er décembre 1944). Il faut attendre 2011 pour que les pensions des Anciens Combattants des troupes coloniales reçoivent une pension équivalente à celle des Anciens Combattants français.

Date de dénomination de la rue : 29/03/2010
