---
layout: post
title:  "Al Pouessi alias Modeste Testas (1765-1868)"
date:   2020-05-10
image: statue-Al-Pouessi.JPG
---

<p class="intro"><span class="dropcap">E</span>lle s’appelait  Al Pouessi, née vers 1765 en Ethiopie. Elle devint Modeste Testas du nom de son propriétaire, marchand et esclavagiste bordelais. Ce  fulgurant raccourci  historique, ce rapt identitaire résume la réalité de l’esclavage qui en l’occurrence s’augmente ici d’une exploitation  sexuelle.</p>


C’est ce personnage que la ville de Bordeaux a choisi deux siècles et demi plus tard d’honorer en érigeant une statue<sup id="ap1"> <a href="#nt1">1</a></sup> à son effigie au pied de la bourse maritime quai Louis XVIII à Bordeaux non sans susciter son lot de critiques.
Marthe Adélaïde Modeste Testas comme l’indique précisément la plaque explicative qui accompagne la -belle- statue de bronze, fut victime d’une razzia et au terme de tribulations dont on sait peu de choses, elle  fut achetée par deux frères, François et Pierre  Testas, négociants à Bordeaux, propriétaires d’une sucrerie à Saint-Domingue. Pierre assurait le commerce à Bordeaux quand François gérait l’exploitation dans les îles. Elle serait alors passée par Bordeaux mais  on n’en trouve pas de trace.

### Affranchie et héritière

En 1781, elle a environ 16 ans quand son « maître » François  Testas l’emmène à Saint-Domingue à Jérémie où se trouve sa propriété. Baptisé Marthe Adelaïde Modeste Testas, elle devient son esclave sexuelle. Il lui fit plusieurs  enfants. La mémoire familiale avec beaucoup de mansuétude assure qu’il «  se serait bien comporté avec elle. » Dans la partie française de Saint-Domingue, on comptait (1789) 30.000 blancs, 30.000 mulâtres et 465.000 esclaves noirs abrités  selon le  père Pierre-François de Charlevoix, dans des maisons qui « ressemblent à des tanières faîtes pour loger des ours. » Saint-Domingue fut le terminal de 78% des 346 expéditions négrières bordelaises<sup id="ap2"> <a href="#nt2">2</a></sup>.
On ignore comment s’est comporté François Testas en 1793 quand les colons ont en quelques sorte livré Saint-Domingue aux Anglais pour se protéger des décrets antiesclavagistes de la révolution française. Toujours est-il qu’en 1795, il partit pour New-York emmenant Modeste et Joseph Lespérance, ses esclaves » domestiques. »  Il mourut la même année après les avoir affranchis et  donné par testament  la main de Modeste à Joseph ainsi que 51 carreaux de terre de l’habitation Testas à Jérémie. Modeste y fit encore 9 enfants avec Joseph et vécue là jusqu’à 105 ans. Dans sa nombreuse descendance, on trouve un petit-fils qui fut un éphémère président de la république haïtienne en 1888/89.  

C’est donc cette statue qui depuis mai 2019 symbolise le passé négrier de Bordeaux.  Karfa Diallo de l’association Mémoires et Partages<sup id="ap3"> <a href="#nt3">3</a></sup> s’interroge sur son blog abrité par le site Médiapart : « Comment «  une affranchie » en 1795 à Philadelphie, 4 ans après la Révolution Haïtienne qui sonnera le glas du système esclavagiste, peut-elle représenter la mémoire de l’esclavage colonial ? On supposerait donc que l’affranchissement des captifs africains était le fondement du projet colonial et capitaliste qui a fait la fortune de Bordeaux »

<p>Retrouvez en podcast ITW Amal Kreishe du PWWSD - Voyage d'Isabelle en Palestine #2 - La statue Modeste Testas <a target="_new" href="https://lacledesondes.fr/emission/le-guide-du-bordeaux-colonial">diffusé sur la Clé des ondes le 18 décembre 2019</a></p>

<div style="style_de_notes">
<p id="nt1"><sup><a href="#ap1">1 </a> la réalisation en fut confiée à un jeune sculpteur haïtien Woodly Caymitte dit Filipo.
</p>
</div>
<div style="style_de_notes">
<p id="nt1"><sup><a href="#ap2">2 </a> Éric Saugera  Bordeaux port négrier XVII-XIXe siècle Karthala
</p>
</div>
<div style="style_de_notes">
<p id="nt1"><sup><a href="#ap3">3 </a> KSD, Karfa Sira Diallo réclame à corps et à cris la création à Bordeaux d’un vrai «  mémorial de la traite des noirs » à l’image de ce qui s’est fait à Nantes.
</p>
</div>
