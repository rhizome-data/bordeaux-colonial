---
layout: post
title:  "Alexis de Tocqueville ou Tocqueville (rue)"
date:   2020-09-04
image: alexis_de_tocqueville.jpg
---

Connu, reconnu et toujours cité pour son ouvrage « De la démocratie en Amérique », Alexis de Tocqueville (1805-1859) est un homme politique considéré comme un précurseur de la sociologie politique.

Généralement présenté comme un libéral et un catholique social, son plaidoyer pour le colonialisme est souvent oublié. C’est comme député qu’il a fait deux voyages en Algérie, et ses rapports, Travail sur l’Algérie (1841) et Rapport sur le projet de loi relatif aux crédits extraordinaires demandés pour l’Algérie (1847) , ont été publiés. Si en 1841 il valide la politique de la terre brûlée de Bugeaud*, il plaide en 1847 pour la construction d’institutions coloniales se gardant de commettre des fautes par excès de rigueur ou laxisme, en évitant de considérer les indigènes comme s’ils étaient nos égaux.