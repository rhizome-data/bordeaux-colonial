---
layout: post
title:  "Olympe de Gouges (1748 - 1793)"
date:   2020-05-10
image: Marie-Olympe-de-Gouges.jpg
---

<p class="intro"><span class="dropcap">N</span>ée à Montauban dans une famille modeste le 7 mai 1748, Marie Gouze, dite Olympe de Gouges, de par sa vie et ses engagements,  devient une figure de l’émancipation féminine, prônant la liberté des moeurs, s’opposant au mariage forcé des filles et plaidant pour le droit au divorce. </p>

La pièce qui rendit célèbre Olympe de Gouges est L’esclavage des noirs, ou l’heureux naufrage, publié sous ce titre en 1792 mais inscrite au répertoire de la Comédie-Française le 30 juin 1785 sous le titre de Zamore et Mirza, ou L’heureux naufrage.

Olympe de Gouges publia en février 1788 des Réflexions sur les hommes nègres,
« L’espèce d’hommes nègres, écrivait-elle avant la Révolution, m’a toujours intéressée à son dé-plorable sort. Ceux que je pus interroger ne satisfirent jamais ma curiosité et mon raisonnement. Ils traitaient ces gens-là de brutes, d’êtres que le Ciel avait maudits ; mais en avançant en âge, je vis clairement que c’était la force et le préjugé qui les avaient condamnés à cet horrible escla-vage, que la Nature n’y avait aucune part et que l’injuste et puissant intérêt des Blancs avait tout fait. »

Olympe de Gouges s’engage dès le début de la révolution française, tout en gardant son indé-pendance et son esprit critique. Elle revendique le droit pour les femmes d’occuper une place à part entière dans leur vie privée et dans la vie politique et publie à ce sujet en 1791, en miroir à la Déclaration des Droits de l’Homme et du Citoyen », une « Déclaration des Droits des Femmes et de la Citoyenne ».

Avec la Révolution française, et malgré les changements politiques, l'idéologie coloniale restait très présente. Olympe de Gouges, soutenue par la Société des amis des Noirs, continua à faire face au harcèlement, aux pressions et même aux menaces. En avril 1790 dans ses adieux aux Français elle annonça qu'elle venait d'écrire une seconde pièce abolitionniste, intitulée le Marché des Noirs. Mais elle la proposa sans succès en décembre de la même année.

Elle sera arrêtée le 20 juillet 1793 pour avoir rédigé un placard fédéraliste à caractère girondin, Les Trois Urnes ou le Salut de la Patrie, jugée le 2 novembre et exécutée sur l’échafaud dès le lendemain.

Date de dénomination de la place : 15 octobre 2018


<p>Retrouvez en podcast Olympe de Gouges - Entretien avec l'écrivaine Lysiane Réginensi-Rolland <a target="_new" href="https://lacledesondes.fr/emission/le-guide-du-bordeaux-colonial">diffusé sur la Clé des ondes le 12 février 2020</a></p>
